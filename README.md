# Elastic plugin for BEANS

`BEANS` is a web-based software for interactive distributed data analysis with a clear interface for querying, filtering, aggregating, and plotting data from an arbitrary number of datasets and tables [https://gitlab.com/beans-code/beans](https://gitlab.com/beans-code/beans).

`Elastic` is distributed search engine [https://www.elastic.co](https://www.elastic.co).

This plugin allow to create search engine of the `BEANS` database in `Elastic` which allows to find any information easily and fast.
