package net.beanscode.plugin.elastic;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

import org.rendersnake.HtmlCanvas;

import net.beanscode.model.notebook.NotebookEntry;
import net.beanscode.model.plugins.Connector;
import net.beanscode.model.plugins.Func;
import net.beanscode.model.plugins.Plugin;
import net.hypki.libs5.db.db.DatabaseProvider;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.search.SearchEngineProvider;
import net.hypki.libs5.search.elastic.ElasticSearchProvider;

public class ElasticPlugin implements Plugin {

	public ElasticPlugin() {
		
	}

	@Override
	public String getName() {
		return "Elastic search engine plugin";
	}

	@Override
	public String getVersion() {
		return "1.0.0";
	}

	@Override
	public String getDescription() {
		return "Elastic search engine is fully distributed search engine - ideal for huge amount of data.";
	}

	@Override
	public String getPanel(UUID userId) throws IOException {
		return new HtmlCanvas()
				.h3()
					.content("Description")
				.p()
					.content(getDescription()).toHtml();
	}

	@Override
	public List<Func> getFuncList() {
		return null;
	}

	@Override
	public List<Class<? extends Connector>> getConnectorClasses() {
		return null;
	}

	@Override
	public List<Class<? extends NotebookEntry>> getNotebookEntries() {
		return null;
	}
	
	@Override
	public List<Class<? extends DatabaseProvider>> getDatabaseProviderList() {
		return null;
	}
	
	@Override
	public List<Class<? extends SearchEngineProvider>> getSearchEngineProviderList() {
		List<Class<? extends SearchEngineProvider>> searchEngines = new ArrayList<>();
		searchEngines.add(ElasticSearchProvider.class);
		return searchEngines;
	}
	
	@Override
	public boolean selfTest(UUID userId, OutputStreamWriter output) throws ValidationException, IOException {
		output.write("TODO");
		return true;
	}
}
